#!/usr/bin/env perl
#
# desktop-switch.pl
# Created by Frost on 2021-12-27
#
# Usage: desktop-switch.pl +|-|<number>
#
# This is free and unencumbered software released into the public domain.
# See COPYING.md for more information.

use strict;
use warnings;
use v5.18;
# switch is experimental, and has warnings plastered all over it to that effect
no warnings "experimental";

use Net::DBus;
use List::Util qw(first);

my $usage = "Usage: desktop-switch.pl +|-|<number>\n";

if (!defined($ARGV[0])) {
	die $usage;
}
if ($ARGV[0] eq "--help") {
	print $usage;
	exit 0;
}

my $bus = Net::DBus->session;

my $kwin = $bus->get_service("org.kde.KWin");
my $desktopManager = $kwin->get_object("/VirtualDesktopManager", "org.freedesktop.DBus.Properties");
my $current = $desktopManager->Get("org.kde.KWin.VirtualDesktopManager", "current");
my @desktopInfos = @{$desktopManager->Get("org.kde.KWin.VirtualDesktopManager", "desktops")};
# array of arrays, like ((0, "[UUID]", "Desktop 1"), ...)
my @desktops = map {
	# get just the ID
	@{$_}[1]
} @desktopInfos;

my $currentIndex = first { $desktops[$_] eq $current } 0..$#desktops;
# say "Current desktop: ", $currentIndex+1;
given ("$ARGV[0]") {
	# what do we do with it?
	when (/^\+$/) { $currentIndex++; }
	when (/^-$/) { $currentIndex--; }
	when (/^[0-9]+$/) { $currentIndex = $_ - 1; }
	default { die $usage }
}

if ($currentIndex < 0) {
	# at the beginning
	say "At beginning, aborting.";
	exit 0;
}
if ($currentIndex > $#desktops) {
	# at the end
	say "At end, aborting.";
	exit 0;
}

my $newDesktop = $desktops[$currentIndex];
$desktopManager->Set("org.kde.KWin.VirtualDesktopManager", "current", $newDesktop);
